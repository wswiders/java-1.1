package com.capgemini.fibonacci.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.fibonacci.Fibonacci;
import com.capgemini.fibonacci.exception.*;

public class FibonacciTest {

	@Test
	public void shouldReturnZeroWhenInputZero() throws Exception {
		assertEquals(0, Fibonacci.fib(0));
	}

	@Test
	public void shouldReturnOneWhenInputOne() throws Exception {
		assertEquals(1, Fibonacci.fib(1));
	}

	@Test
	public void shouldReturn55WhenInput10() throws Exception {
		assertEquals(55, Fibonacci.fib(10));
	}

	@Test(expected = MinusValueException.class)
	public void shouldReturnMinusValueExcepWhenInputNegative() throws Exception {
		long fib = Fibonacci.fib(-2);
	}

	@Test(expected = TooBigNumberException.class)
	public void shouldReturnTooBigNumberExcepWhenInputTooBig() throws Exception {
		long fib = Fibonacci.fib(93);
	}
}
