package com.capgemini.fibonacci.exception;

public class MinusValueException extends Exception{

	public MinusValueException(String msg){
		super(msg);
	}
	
}
