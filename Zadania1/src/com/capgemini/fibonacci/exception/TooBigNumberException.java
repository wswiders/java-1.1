package com.capgemini.fibonacci.exception;

public class TooBigNumberException extends Exception {
	
	public TooBigNumberException(String msg){
		super(msg);
	}

}
