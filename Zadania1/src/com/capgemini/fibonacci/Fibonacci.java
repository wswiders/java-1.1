package com.capgemini.fibonacci;

import com.capgemini.fibonacci.exception.*;

public class Fibonacci {

	public static long fib(int n) throws Exception {

		if (n < 0) {
			throw new MinusValueException("Wprowadzono wartosc ujemna.");
		}

		if (n > 92) {
			throw new TooBigNumberException("Wynik poza zakresem.");
		}
		
		long[] fibNumber = new long[n + 1];
		fibNumber[0] = 0;
		if (n > 0)
			fibNumber[1] = 1;
		if (n > 1) {
			for (int i = 2; i <= n; i++) {
				fibNumber[i] = fibNumber[i - 1] + fibNumber[i - 2];
			}
		}
		return fibNumber[n];
	}
}